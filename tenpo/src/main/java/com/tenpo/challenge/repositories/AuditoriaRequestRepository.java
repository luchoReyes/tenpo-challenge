package com.tenpo.challenge.repositories;

import com.tenpo.challenge.entities.AuditoriaRequestEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

public interface AuditoriaRequestRepository extends CrudRepository<AuditoriaRequestEntity, Integer> {


    @Query("SELECT AR " +
            "FROM AuditoriaRequestEntity AR " +
            "WHERE (AR.fechaCreacion BETWEEN :fechaHoraDesde AND :fechaHoraHasta) " +
            "ORDER BY AR.fechaCreacion DESC")
    Page<AuditoriaRequestEntity> obtenerHistorial(LocalDateTime fechaHoraDesde,
                                                  LocalDateTime fechaHoraHasta,
                                                  Pageable pageable);


}
