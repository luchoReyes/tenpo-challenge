package com.tenpo.challenge.repositories;


import com.tenpo.challenge.entities.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    boolean existsByUsername(String username);

    UsuarioEntity findFirstByUsername(String username);
}
