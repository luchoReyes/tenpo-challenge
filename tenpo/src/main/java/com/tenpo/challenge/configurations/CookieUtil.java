package com.tenpo.challenge.configurations;

import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;

@Component
public class CookieUtil {

    public static final String TENPO = "tenpo";
    private static final Integer SEGUNDOS = 1;
    private static final Integer MINUTOS = 60 * SEGUNDOS;
    private static final Integer HORAS = 60 * MINUTOS;
    private static final Integer DIAS = 24 * HORAS;
    public static final Integer MES = 30 * DIAS;

    public Cookie generarCookie(String token) {
        Cookie cookie = new Cookie("tenpo", token);
        cookie.setMaxAge(MES);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        return cookie;
    }

    public Cookie eliminarCookie(String token) {
        Cookie cookie = generarCookie(token);
        cookie.setMaxAge(0);
        return cookie;
    }

}
