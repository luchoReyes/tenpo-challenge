package com.tenpo.challenge.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "auditoria_requests")
public class AuditoriaRequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "uri")
    private String uri;
    @Column(name = "status")
    private Integer status;
    @Column(name = "response", columnDefinition = "TEXT")
    private String response;
    @Column(name = "fecha_creacion")
    private LocalDateTime fechaCreacion;

}
