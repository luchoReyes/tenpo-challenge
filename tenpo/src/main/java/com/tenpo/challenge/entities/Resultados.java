package com.tenpo.challenge.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@AllArgsConstructor
public class Resultados {
    private Integer paginas;
    private Long total;
    private Integer totalResultados;
    private List<?> resultados;
}
