package com.tenpo.challenge.services;

import com.google.gson.Gson;
import com.tenpo.challenge.entities.AuditoriaRequestEntity;
import com.tenpo.challenge.entities.Resultados;
import com.tenpo.challenge.repositories.AuditoriaRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

@Service
public class AuditoriaRequestService {

    private static final Integer SIZE = 5;
    private static final LocalDateTime FECHA_MINIMA = LocalDateTime.of(1000, Month.JANUARY, 1, 0, 0);
    Gson gson = new Gson();

    @Autowired
    AuditoriaRequestRepository auditoriaRequestRepository;

    @Async
    public void auditarRequest(Object body, HttpServletResponse response, HttpServletRequest request) {

        Integer status = response.getStatus();
        if (esAuditoriaRequest(status)) {
            AuditoriaRequestEntity auditoria = new AuditoriaRequestEntity();
            auditoria.setUri(request.getRequestURI());
            auditoria.setStatus(status);
            auditoria.setResponse(obtenerBody(body));
            auditoria.setFechaCreacion(LocalDateTime.now());
            auditoriaRequestRepository.save(auditoria);
        }
    }

    public Resultados obtenerHistorial(LocalDateTime fechaHoraDesde, LocalDateTime fechaHoraHasta, Integer pagina) {
        fechaHoraDesde = fechaHoraDesde == null ? FECHA_MINIMA : fechaHoraDesde;
        fechaHoraHasta = fechaHoraHasta == null ? LocalDateTime.now() : fechaHoraHasta;
        Pageable pageable = PageRequest.of(pagina, SIZE);
        Page<AuditoriaRequestEntity> page = auditoriaRequestRepository.obtenerHistorial(fechaHoraDesde, fechaHoraHasta, pageable);
        validarContenido(page.getContent());
        return new Resultados(page.getTotalPages(), page.getTotalElements(), page.getContent().size(), page.getContent());
    }

    private void validarContenido(List<AuditoriaRequestEntity> auditorias) {
        if (auditorias.isEmpty()) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "No existen resultados para la búsqueda");
        }
    }

    private String obtenerBody(Object body) {
        return gson.toJson(body);
    }

    private boolean esAuditoriaRequest(Integer status) {
        return status.equals(HttpStatus.OK.value()) ||
                status.equals(HttpStatus.CREATED.value()) ||
                status.equals(HttpStatus.NO_CONTENT.value());
    }

}
