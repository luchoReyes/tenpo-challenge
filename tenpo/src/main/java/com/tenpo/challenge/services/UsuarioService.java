package com.tenpo.challenge.services;

import com.tenpo.challenge.entities.UsuarioEntity;
import com.tenpo.challenge.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsuarioEntity usuario = usuarioRepository.findFirstByUsername(username);
        return new org.springframework.security.
                core.userdetails.User(usuario.getUsername(), usuario.getPassword(), new ArrayList<>());
    }

    public UsuarioEntity getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = null;
        if (authentication != null) {
            username = ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return username == null ? null : obtenerUsuario(username);
    }

    public boolean existeUsuario(String username) {
        return usuarioRepository.existsByUsername(username);
    }

    public void registrarUsuario(String username, String passowrd) {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setUsername(username);
        usuario.setPassword(passowrd);
        usuario.setFechaCreacion(LocalDateTime.now());
        usuarioRepository.save(usuario);
    }

    public UsuarioEntity obtenerUsuario(String username) {
        return usuarioRepository.findFirstByUsername(username);
    }

}
