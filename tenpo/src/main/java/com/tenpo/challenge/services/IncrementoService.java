package com.tenpo.challenge.services;

import com.tenpo.challenge.dtos.PorcentajeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import reactor.util.retry.Retry;

import java.time.Duration;

@Service
@Slf4j
@EnableScheduling
public class IncrementoService {

    @Value("${BASE_PATH}")
    private String basePath;
    @Value("${INCREMENTO_PATH}")
    private String incrementoPath;
    @Value("${MAX_RETRY}")
    private Integer maxRetry;
    @Value("${DURATION_REQUEST}")
    private Integer durationRequest;

    private final int SEGUNDOS = 1000;
    private final int MINUTOS = 60 * SEGUNDOS;
    private final int DELAY = 30 * MINUTOS;

    public Double porcentaje = null;

    @Autowired
    WebClient.Builder webClient;

    public Double calcularIncremento(Double valor1, Double valor2) {
        validarPorcentaje();
        return (valor1 + valor2) * porcentaje;
    }

    @Scheduled(initialDelay = DELAY, fixedRate = DELAY)
    private void obtenerPorcentaje() {
        PorcentajeDTO porcentaje = webClient.build()
                .get()
                .uri(basePath + incrementoPath)
                .retrieve()
                .bodyToFlux(PorcentajeDTO.class)
                .retryWhen(Retry.backoff(maxRetry, Duration.ofMillis(durationRequest))
                        .filter(throwable -> throwable instanceof WebClientRequestException)
                        .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> retrySignal.failure()))
                .blockFirst();


        if (existeNuevoPorcentaje(porcentaje)) {
            this.porcentaje = 1.0 + (porcentaje.getPorcentaje() / 100);
        }
    }

    private void validarPorcentaje() {
        if (this.porcentaje == null) {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR,
                    "No es posible calcular el incremento");
        }
    }

    private boolean existeNuevoPorcentaje(PorcentajeDTO porcentaje) {
        return porcentaje != null && porcentaje.getPorcentaje() != null;
    }

}
