package com.tenpo.challenge.services;

import com.tenpo.challenge.configurations.JwtUtil;
import com.tenpo.challenge.entities.UsuarioEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

@Service
public class AuthService {

    @Autowired
    UsuarioService usuarioService;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtil jwtUtil;

    public String login(String username, String password) {
        validarLogin();
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
            User user = (User) authentication.getPrincipal();
            UsuarioEntity usuarioEntity = usuarioService.obtenerUsuario(user.getUsername());
            return jwtUtil.generateToken(usuarioEntity);
        } catch (InternalAuthenticationServiceException | BadCredentialsException ex) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND, "Usuario y/o contraseña incorrectos");
        }

    }

    public void signUp(String username, String password) {
        validatesingUp(username);
        String encodPassword = bCryptPasswordEncoder.encode(password);
        usuarioService.registrarUsuario(username, encodPassword);
    }

    private void validarLogin() {
        if (usuarioService.getCurrentUser() != null) {
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "Ya existe un usuario logueado");
        }
    }

    private void validatesingUp(String username) {
        if (usuarioService.existeUsuario(username)) {
            throw new IllegalArgumentException("Usuario existente");
        }
    }
}
