package com.tenpo.challenge.dtos;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PorcentajeDTO {

    private Double porcentaje;

}
