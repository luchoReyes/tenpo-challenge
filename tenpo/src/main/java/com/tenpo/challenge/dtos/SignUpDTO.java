package com.tenpo.challenge.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SignUpDTO {

    @NotNull(message = "El usuario no puede ser nulo")
    private String username;
    @NotNull(message = "La contraseña no puede ser nula")
    private String password;
}
