package com.tenpo.challenge.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
public class IncrementoDTO {

    private Double Incremento;

}
