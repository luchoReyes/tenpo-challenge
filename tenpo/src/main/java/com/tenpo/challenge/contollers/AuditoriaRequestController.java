package com.tenpo.challenge.contollers;

import com.tenpo.challenge.entities.Resultados;
import com.tenpo.challenge.services.AuditoriaRequestService;
import com.tenpo.challenge.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@RequestMapping(value = "/request-audits")
public class AuditoriaRequestController {

    @Autowired
    AuditoriaRequestService auditoriaRequestService;
    @Autowired
    UsuarioService usuarioService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> obtenerAuditorias(@Nullable @RequestParam("fechaHoraDesde")
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                       LocalDateTime fechaHoraDesde,
                                               @Nullable @RequestParam("fechaHoraHasta")
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                       LocalDateTime fechaHoraHasta,
                                               @RequestParam("pagina") Integer pagina) {
        Resultados resultados = auditoriaRequestService.obtenerHistorial(fechaHoraDesde, fechaHoraHasta, pagina);
        return new ResponseEntity<>(resultados, HttpStatus.OK);
    }
}
