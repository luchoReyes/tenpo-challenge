package com.tenpo.challenge.contollers;

import com.tenpo.challenge.configurations.CookieUtil;
import com.tenpo.challenge.dtos.LoginDTO;
import com.tenpo.challenge.dtos.ResponseDTO;
import com.tenpo.challenge.dtos.SignUpDTO;
import com.tenpo.challenge.services.AuditoriaRequestService;
import com.tenpo.challenge.services.AuthService;
import com.tenpo.challenge.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
public class AuthController {

    @Autowired
    AuthService authService;
    @Autowired
    CookieUtil cookieUtil;
    @Autowired
    UsuarioService usuarioService;

    @RequestMapping(value = "/log-in", method = RequestMethod.POST)
    public ResponseEntity<?> login(@Valid @RequestBody LoginDTO loginDTO,
                                   HttpServletRequest request,
                                   HttpServletResponse response) {
        String token = authService.login(loginDTO.getUsername(), loginDTO.getPassword());
        response.addCookie(cookieUtil.generarCookie(token));
        ResponseDTO responseDTO = ResponseDTO.builder().mensaje("Bienvenido").build();
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@Valid @RequestBody SignUpDTO signUpDTO) {
        authService.signUp(signUpDTO.getUsername(), signUpDTO.getPassword());
        return new ResponseEntity<>(ResponseDTO.builder().mensaje("Tu usuario se registro con éxito").build(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/log-out", method = RequestMethod.POST)
    public ResponseEntity<?> cerrarSesion(@CookieValue(CookieUtil.TENPO) String token, HttpServletResponse response) {
        response.addCookie(cookieUtil.eliminarCookie(token));
        return new ResponseEntity<>(ResponseDTO.builder().mensaje("Que vuelvas pronto").build(), HttpStatus.OK);
    }
}
