package com.tenpo.challenge.services;

import com.tenpo.challenge.dtos.ResponseDTO;
import com.tenpo.challenge.entities.AuditoriaRequestEntity;
import com.tenpo.challenge.entities.Resultados;
import com.tenpo.challenge.repositories.AuditoriaRequestRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuditoriaRequestServiceTest {

    @Autowired
    AuditoriaRequestService auditoriaRequestService;
    @MockBean
    AuditoriaRequestRepository auditoriaRequestRepository;
    @MockBean
    HttpServletRequest request;
    @MockBean
    HttpServletResponse response;
    @MockBean
    Page<AuditoriaRequestEntity> page;
    @MockBean
    List<AuditoriaRequestEntity> auditoriaRequestList;
    @Mock
    AuditoriaRequestEntity auditoriaRequestEntity;

    @Before
    public void setUp() {
        mock(AuditoriaRequestService.class);
    }

    @Test
    public void audtoriaRequestTestHttpStatusOK() {
        when(auditoriaRequestRepository.save(any())).thenReturn(auditoriaRequestEntity);
        Mockito.when(request.getRequestURI()).thenReturn("/test");
        Mockito.when(response.getStatus()).thenReturn(200);
        ResponseDTO object = new ResponseDTO("test");
        auditoriaRequestService.auditarRequest(object, response, request);
        verify(auditoriaRequestRepository, times(0)).save(Mockito.any(AuditoriaRequestEntity.class));
    }

    @Test
    public void audtoriaRequestTesHttpStatusCREATED() {
        when(auditoriaRequestRepository.save(any())).thenReturn(auditoriaRequestEntity);
        Mockito.when(request.getRequestURI()).thenReturn("/test");
        Mockito.when(response.getStatus()).thenReturn(201);
        ResponseDTO object = new ResponseDTO("test");
        auditoriaRequestService.auditarRequest(object, response, request);
        verify(auditoriaRequestRepository, times(0)).save(Mockito.any(AuditoriaRequestEntity.class));
    }

    @Test
    public void audtoriaRequestTesHttpStatusNO_CONTENT() {
        when(auditoriaRequestRepository.save(any())).thenReturn(auditoriaRequestEntity);
        Mockito.when(request.getRequestURI()).thenReturn("/test");
        Mockito.when(response.getStatus()).thenReturn(204);
        ResponseDTO object = new ResponseDTO("test");
        auditoriaRequestService.auditarRequest(object, response, request);
        verify(auditoriaRequestRepository, times(0)).save(Mockito.any(AuditoriaRequestEntity.class));
    }


    @Test
    public void obtenerHistorialFullParametersTestOk() {
        Mockito.when(page.getTotalPages()).thenReturn(1);
        Mockito.when(page.getTotalElements()).thenReturn(2L);
        Mockito.when(page.getContent()).thenReturn(auditoriaRequestList);
        Mockito.when(page.getContent().size()).thenReturn(3);
        Mockito.when(auditoriaRequestRepository.obtenerHistorial(any(), any(), any()))
                .thenReturn(page);


        Resultados result = auditoriaRequestService.obtenerHistorial(LocalDateTime.now(), LocalDateTime.now(), 0);

        Assertions.assertEquals(result.getTotal(), 2L);
        Assertions.assertEquals(result.getTotalResultados(), 3);
        Assertions.assertEquals(result.getResultados(), auditoriaRequestList);
        Assertions.assertEquals(result.getPaginas(), 1);
    }

    @Test
    public void obtenerHistorialHttpServerErrorExceptionTest() {
        Mockito.when(page.getTotalPages()).thenReturn(1);
        Mockito.when(page.getTotalElements()).thenReturn(2L);
        Mockito.when(auditoriaRequestList.isEmpty()).thenReturn(true);
        Mockito.when(page.getContent()).thenReturn(auditoriaRequestList);
        Mockito.when(page.getContent().size()).thenReturn(3);
        Mockito.when(auditoriaRequestRepository.obtenerHistorial(any(), any(), any()))
                .thenReturn(page);

        Assertions.assertThrows(HttpServerErrorException.class, () -> {
            auditoriaRequestService.obtenerHistorial(LocalDateTime.now(), LocalDateTime.now(), 0);
        });
    }


}
