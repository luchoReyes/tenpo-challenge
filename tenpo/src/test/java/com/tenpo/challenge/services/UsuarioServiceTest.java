package com.tenpo.challenge.services;

import com.tenpo.challenge.entities.UsuarioEntity;
import com.tenpo.challenge.repositories.UsuarioRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioServiceTest {

    @Autowired
    UsuarioService usuarioService;
    @MockBean
    UsuarioRepository usuarioRepository;
    @Mock
    UsuarioEntity usuarioEntity;
    @MockBean
    Authentication authentication;
    @MockBean
    SecurityContext securityContext;
    @MockBean
    SecurityContextHolderStrategy securityContextHolderStrategy;
    @MockBean
    UserDetails UserDetails;
    @Mock
    UsuarioEntity usuario;

    @Before
    public void SetUp() {
        Mockito.mock(UsuarioService.class);
    }

    @Test
    public void loadUserByUsernameOk() {
        when(usuarioEntity.getUsername()).thenReturn("test");
        when(usuarioEntity.getPassword()).thenReturn("testpassword");
        when(usuarioRepository.findFirstByUsername(any())).thenReturn(usuarioEntity);

        UserDetails result = usuarioService.loadUserByUsername("test");

        Assertions.assertEquals("test", result.getUsername());
        Assertions.assertEquals("testpassword", result.getPassword());
        Assertions.assertTrue(result.getAuthorities().isEmpty());
    }

    @Test
    public void getCurrentUserOk() {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(UserDetails);
        when(UserDetails.getUsername()).thenReturn("test");
        when(usuarioRepository.findFirstByUsername(any())).thenReturn(usuario);
        UsuarioEntity result = usuarioService.getCurrentUser();
        Assertions.assertNotNull(result);
        verify(usuarioRepository, times(1)).findFirstByUsername(any());
    }

    @Test
    public void getCurrentUserNull() {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(null);
        UsuarioEntity result = usuarioService.getCurrentUser();
        Assertions.assertNull(result);
    }

    @Test
    public void existeUsuarioOk() {
        when(usuarioRepository.existsByUsername("test")).thenReturn(true);

        Assertions.assertTrue(usuarioService.existeUsuario("test"));

    }

    @Test
    public void registrarUsuarioOk() {
        when(usuarioRepository.save(any())).thenReturn(usuarioEntity);
        usuarioService.registrarUsuario("test", "test");
        verify(usuarioRepository, times(1)).save(any(UsuarioEntity.class));

    }


}
