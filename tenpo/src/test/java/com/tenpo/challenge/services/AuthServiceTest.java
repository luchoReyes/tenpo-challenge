package com.tenpo.challenge.services;


import com.tenpo.challenge.configurations.JwtUtil;
import com.tenpo.challenge.entities.UsuarioEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpServerErrorException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthServiceTest {

    @Autowired
    AuthService authService;
    @MockBean
    UsuarioService usuarioService;
    @MockBean
    AuthenticationManager authenticationManager;
    @MockBean
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @MockBean
    Authentication authentication;
    @MockBean
    JwtUtil jwtUtil;
    @Mock
    UsuarioEntity usuarioEntity;
    @Mock
    User user;

    @Before
    public void SetUp() {
        mock(AuthServiceTest.class);
    }

    @Test
    public void loginTestOK() {
        when(usuarioService.getCurrentUser()).thenReturn(null);
        when(authentication.getPrincipal()).thenReturn(user);
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        when(usuarioService.obtenerUsuario(any())).thenReturn(usuarioEntity);
        when(jwtUtil.generateToken(any())).thenReturn("jwttoken");

        String result = authService.login("test", "test");
        Assertions.assertEquals(result, "jwttoken");
    }

    @Test
    public void loginTestLoggedUser() {
        when(usuarioService.getCurrentUser()).thenReturn(usuarioEntity);
        Assertions.assertThrows(HttpServerErrorException.class, () -> {
            authService.login("test", "test");
        });
    }

    @Test
    public void loginTestInternalAuthenticationServiceException() {
        when(usuarioService.getCurrentUser()).thenReturn(null);
        when(authenticationManager.authenticate(any())).thenReturn(authentication);
        when(authentication.getPrincipal()).thenThrow(InternalAuthenticationServiceException.class);
        Assertions.assertThrows(HttpServerErrorException.class, () -> {
            authService.login("test", "test");
        });
    }

    @Test
    public void signUpTestOK() {
        when(usuarioService.existeUsuario(any())).thenReturn(false);
        when(bCryptPasswordEncoder.encode(any())).thenReturn("encriptada");
        authService.signUp("test", "test");
        verify(usuarioService, times(1)).registrarUsuario(any(), any());
    }

    @Test
    public void signUpTestIllegalArgumentException() {
        when(usuarioService.existeUsuario(any())).thenReturn(true);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            authService.signUp("test", "test");
        });
    }

}
