package com.tenpo.challenge.services;


import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpServerErrorException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IncrementoServiceTest {

    @Autowired
    IncrementoService incrementoService;

    @Before
    public void setUp() {
        Mockito.mock(IncrementoService.class);
    }

    @Test
    public void calcularIncrementoNullValue() {
        incrementoService.porcentaje = null;
        HttpServerErrorException e =assertThrows(HttpServerErrorException.class, () -> {
            incrementoService.calcularIncremento(1.0, 1.0);
        });
        Assertions.assertEquals("No es posible calcular el incremento", e.getStatusText());
    }

    @Test
    public void calcularIncrementoOK() {
        incrementoService.porcentaje = 1.0;
        Double expected = 2.0;
        Double result = incrementoService.calcularIncremento(1.0, 1.0);
        assertEquals(expected, result);
    }




//    @Test
//    public void obtenerPorcentajeOK() {
//        PorcentajeDTO mockPorcentaje = PorcentajeDTO.builder().porcentaje(0.5).build();
//        when(webClientMock.get()).thenReturn(requestHeadersUriMock);
//        when(requestHeadersUriMock.uri("/porcentaje-actual")).thenReturn(requestHeadersMock);
//        when(requestHeadersMock.retrieve()).thenReturn(responseMock);
//        when(responseMock.bodyToFlux(PorcentajeDTO.class)).thenReturn(Flux.just(mockPorcentaje));
//        incrementoService.obtenerPorcentaje();
//    }


}
