package com.tenpo.challenge.controllers;

import com.tenpo.challenge.contollers.IncrementoController;
import com.tenpo.challenge.services.AuditoriaRequestService;
import com.tenpo.challenge.services.IncrementoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
public class IncrementoControllerTest {


    private MockMvc mockMvc;
    private static final String URI = "/increments";
    @Autowired
    WebApplicationContext webApplicationContext;
    @Autowired
    IncrementoController incrementoController;
    @MockBean
    IncrementoService incrementoService;
    @MockBean
    AuditoriaRequestService auditoriaRequestService;

    @Before
    public void setUp() {
        Mockito.mock(IncrementoService.class);
    }

    @Test
    public void calcularIncrementoTestOK() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        Mockito.when(incrementoService.calcularIncremento(any(), any())).thenReturn(1.0);
        mockMvc.perform(MockMvcRequestBuilders.get(URI)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("valor1", "1")
                        .param("valor2", "1")
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.incremento", is(1.0)));
    }

    @Test
    public void calcularIncrementoMissingServletRequestParameterExceptionTest() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String expected = "El parámetro [valor1] es requerido";
        mockMvc
                .perform(MockMvcRequestBuilders.get(URI)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("valor2", "1")
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.mensaje", is(expected)));

    }

    @Test
    public void calcularIncrementoIllegalArgumentExceptionTest() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        String expected = "Ningún valor puede ser negativo";
        mockMvc
                .perform(MockMvcRequestBuilders.get(URI)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("valor1", "-1")
                        .param("valor2", "1")
                ).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.mensaje", is(expected)));

    }

    @Test
    public void calcularIncrementoHttpServerErrorExceptionTest() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        Mockito.when(incrementoService.calcularIncremento(any(), any())).thenThrow(
                new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "No es posible calcular el incremento"));
        String expected = "No es posible calcular el incremento";
        mockMvc
                .perform(MockMvcRequestBuilders.get(URI)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("valor1", "1")
                        .param("valor2", "1")
                ).andExpect(status().is5xxServerError())
                .andExpect(jsonPath("$.mensaje", is(expected)));

    }

}
