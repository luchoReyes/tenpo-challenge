package com.tenpo.challenge.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenpo.challenge.configurations.CookieUtil;
import com.tenpo.challenge.contollers.AuthController;
import com.tenpo.challenge.dtos.LoginDTO;
import com.tenpo.challenge.services.AuditoriaRequestService;
import com.tenpo.challenge.services.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import javax.servlet.http.Cookie;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthControllerTest {

    private static final String LOGIN_URI = "/log-in";
    private static final String SIGN_UP_URI = "/sign-up";
    private static final String LOGOUT_URI = "/log-out";

    private MockMvc mockMvc;
    @MockBean
    AuthService authService;
    @MockBean
    AuditoriaRequestService auditoriaRequestService;
    @MockBean
    CookieUtil cookieUtil;
    @Autowired
    AuthController authController;
    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void SetUp() {
        Mockito.mock(AuthController.class);
    }

    @Test
    public void loginTestOk() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setUsername("test");
        login.setPassword("test-password");
        Cookie cookie = new Cookie("tenpo", "test");
        Mockito.when(cookieUtil.generarCookie(any())).thenReturn(cookie);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(LOGIN_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(cookie().exists("tenpo"))
                .andExpect(jsonPath("$.mensaje", is("Bienvenido")));

    }

    @Test
    public void loginTestUsernameNull() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setPassword("test-password");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(LOGIN_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("El usuario no puede ser nulo")));

    }

    @Test
    public void loginTestPasswordNull() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setUsername("test");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(LOGIN_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("La contraseña no puede ser nula")));

    }

    @Test
    public void signUpTestOk() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setUsername("test");
        login.setPassword("test-password");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(SIGN_UP_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isCreated())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("Tu usuario se registro con éxito")));

    }

    @Test
    public void signUpTestUsernameNull() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setPassword("test-password");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        doCallRealMethod().when(auditoriaRequestService)
                .auditarRequest(any(), any(), any());
        mockMvc.perform(
                        post(SIGN_UP_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("El usuario no puede ser nulo")));

    }

    @Test
    public void signUpTestPasswordNull() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setUsername("test");
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(SIGN_UP_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(new ObjectMapper().writeValueAsString(login)))
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("La contraseña no puede ser nula")));

    }

    @Test
    public void logoutTestOk() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setUsername("test");
        login.setPassword("");
        Cookie cookie = new Cookie("tenpo", "test");
        Mockito.when(cookieUtil.eliminarCookie(any())).thenReturn(cookie);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc.perform(
                        post(LOGOUT_URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .cookie(cookie))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.mensaje", is("Que vuelvas pronto")));

    }

}
