package com.tenpo.challenge.controllers;

import com.tenpo.challenge.configurations.CookieUtil;
import com.tenpo.challenge.contollers.AuthController;
import com.tenpo.challenge.entities.AuditoriaRequestEntity;
import com.tenpo.challenge.entities.Resultados;
import com.tenpo.challenge.services.AuditoriaRequestService;
import com.tenpo.challenge.services.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
public class AuditoriaRequestControllerTest {

    private static final String URI = "/request-audits";
    private MockMvc mockMvc;
    @Autowired
    WebApplicationContext webApplicationContext;
    @MockBean
    AuditoriaRequestService auditoriaRequestService;
    @Mock
    Resultados resultados;

    @Before
    public void SetUp() {
        Mockito.mock(AuthController.class);
    }


    @Test
    public void obtenerAuditoriasTestOk() throws Exception {
        doCallRealMethod().when(auditoriaRequestService)
                .auditarRequest(any(), any(), any());
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        mockMvc
                .perform(MockMvcRequestBuilders.get(URI)
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .param("fechaHoraDesde", "2000-01-01T05:24:00")
                        .param("fechaHoraHasta", "2000-01-01T05:24:00")
                        .param("pagina", "1")
                )
                .andExpect(status().isOk());

        verify(auditoriaRequestService, times(1)).obtenerHistorial(any(),any(),any());

    }

}
