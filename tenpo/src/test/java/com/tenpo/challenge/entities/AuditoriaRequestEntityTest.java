package com.tenpo.challenge.entities;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuditoriaRequestEntityTest {

    @Test
    public void auditoriaRequestEntityTest() {

        AuditoriaRequestEntity auditoria = new AuditoriaRequestEntity();
        auditoria.setUri("/test");
        auditoria.setStatus(200);
        auditoria.setFechaCreacion(LocalDateTime.now());
        auditoria.setResponse("{test: test}");

        Assertions.assertNotNull(auditoria.getUri());
        Assertions.assertNotNull(auditoria.getStatus());
        Assertions.assertNotNull(auditoria.getFechaCreacion());
        Assertions.assertNotNull(auditoria.getResponse());
    }
}
