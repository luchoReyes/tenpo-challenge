package com.tenpo.challenge.entities;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsuarioEntityTest {


    @Test
    public void usuarioEntityTest() {

        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setId(1);
        usuario.setUsername("test");
        usuario.setPassword("test-password");
        usuario.setFechaCreacion(LocalDateTime.now());

        Assertions.assertNotNull(usuario.getId());
        Assertions.assertNotNull(usuario.getUsername());
        Assertions.assertNotNull(usuario.getPassword());
        Assertions.assertNotNull(usuario.getFechaCreacion());
    }

}
