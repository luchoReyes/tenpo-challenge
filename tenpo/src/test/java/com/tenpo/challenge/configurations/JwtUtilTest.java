package com.tenpo.challenge.configurations;

import com.tenpo.challenge.entities.UsuarioEntity;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JwtUtilTest {

    @Autowired
    JwtUtil jwtUtil;
    @Mock
    UsuarioEntity usuario;


    @Test
    public void getUsernameFromToken() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNjYwNjY5NjkzLCJleHAiOjE2NjMyNjE2OTN9.MsYEeHX8aNQQBRl5Ulj_P0vgq3pPAlaeGVraTpz0VMDqUL1caC5--Sp_h5rrqk7IBkiiQlizW3xWihu41zu8SQ";
        String result = jwtUtil.getUsernameFromToken(token);
        Assertions.assertEquals(result, "test");

    }

    @Test
    public void generateTokenOk() {
        Mockito.when(usuario.getUsername()).thenReturn("test");
        Assertions.assertNotNull(jwtUtil.generateToken(usuario));
    }

}
