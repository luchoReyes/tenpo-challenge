package com.tenpo.challenge.configurations;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.Cookie;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CookieUtilTest {

    @Autowired
    CookieUtil cookieUtil;

    @Test
    public void generarCookieOk() {
        Cookie result = cookieUtil.generarCookie("token-test");
        Assertions.assertEquals(result.getName(), "tenpo");
        Assertions.assertEquals(result.getValue(), "token-test");
    }

    @Test
    public void eliminarCookieOk() {
        Cookie result = cookieUtil.eliminarCookie("token-test");
        Assertions.assertEquals(result.getMaxAge(), 0);
        Assertions.assertEquals(result.getValue(), "token-test");
    }

}
