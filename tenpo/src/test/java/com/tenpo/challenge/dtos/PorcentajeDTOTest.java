package com.tenpo.challenge.dtos;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PorcentajeDTOTest {


    @Test
    public void PorcentajeDTOTest() {
        PorcentajeDTO porcentaje1 = new PorcentajeDTO();
        PorcentajeDTO porcentaje2 = new PorcentajeDTO(1.0);
        porcentaje1.setPorcentaje(1.0);
        Assertions.assertEquals(porcentaje1.getPorcentaje(), porcentaje2.getPorcentaje());
    }
}
