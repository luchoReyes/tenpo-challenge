package com.tenpo.challenge.handlers;

import com.tenpo.challenge.configurations.JwtUtil;
import com.tenpo.challenge.services.UsuarioService;
import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JwtRequestHandlerTest {

    @MockBean
    JwtUtil jwtUtil;
    @MockBean
    UsuarioService usuarioService;
    @MockBean
    HttpServletResponse response;
    @MockBean
    HttpServletRequest request;
    @MockBean
    FilterChain filterChain;
    @MockBean
    Authentication authentication;
    @MockBean
    SecurityContext securityContext;
    @MockBean
    SecurityContextHolderStrategy securityContextHolderStrategy;
    @MockBean
    UserDetails userDetails;
    @Autowired
    JwtRequestHandler jwtRequestHandler;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;

    @Test
    public void doFilterInternalTestOk() throws ServletException, IOException {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("test");
        Cookie cookie = new Cookie("tenpo", "test-cookie");
        Mockito.when(request.getCookies()).thenReturn(Arrays.array(cookie));
        Mockito.when(jwtUtil.getUsernameFromToken(any()))
                .thenReturn("test-token");
        Mockito.when(jwtUtil.isTokenExpired(any())).thenReturn(true);
        Mockito.when(usuarioService.loadUserByUsername(any())).thenReturn(userDetails);
        jwtRequestHandler.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void doFilterInternalTestTokenExpired() throws ServletException, IOException {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(null);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("test");
        Cookie cookie = new Cookie("tenpo", "test-cookie");
        Mockito.when(request.getCookies()).thenReturn(Arrays.array(cookie));
        Mockito.when(jwtUtil.getUsernameFromToken(any()))
                .thenReturn("test-token");
        Mockito.when(jwtUtil.isTokenExpired(any())).thenReturn(false);
        Mockito.when(usuarioService.loadUserByUsername(any())).thenReturn(userDetails);
        jwtRequestHandler.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void doFilterInternalTestBadCredentials() {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getPrincipal()).thenReturn(userDetails);
        when(userDetails.getUsername()).thenReturn("test");
        Cookie cookie = new Cookie("tenpo", "test-cookie");
        Mockito.when(request.getCookies()).thenReturn(Arrays.array(cookie));
        Mockito.when(jwtUtil.getUsernameFromToken(any()))
                .thenThrow(new IllegalArgumentException());
        Assertions.assertThrows(BadCredentialsException.class, () -> {
            jwtRequestHandler.doFilterInternal(request, response, filterChain);
        });
    }

    @Test
    public void doFilterInternalTest() throws ServletException, IOException {
        SecurityContextHolder.setContextHolderStrategy(securityContextHolderStrategy);
        securityContextHolderStrategy.setContext(securityContext);
        when(securityContextHolderStrategy.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(null);
        when(userDetails.getUsername()).thenReturn("test");
        Cookie cookie = new Cookie("tenpo", "test-cookie");
        Mockito.when(request.getCookies()).thenReturn(Arrays.array(cookie));
        Mockito.when(jwtUtil.getUsernameFromToken(any()))
                .thenReturn("test-token");
        Mockito.when(jwtUtil.isTokenExpired(any())).thenReturn(true);
        Mockito.when(usuarioService.loadUserByUsername(any())).thenReturn(userDetails);
        jwtRequestHandler.doFilterInternal(request, response, filterChain);
    }

    @Test
    public void logoutTestOkError() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        AuthenticationException ex = new AuthenticationCredentialsNotFoundException("");
        authenticationEntryPoint.commence(request, response, ex);
        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, response.getStatus());
    }

}
