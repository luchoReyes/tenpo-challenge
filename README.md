# Tenpo - Challenge

Challenge solicitado por el equipo técnico de **Tenpo**.

## Diagrama Arquitectura

![alt-text](docs/assets/arquitectura.png )

### Tenpo (puerto: 8080)
- **Requests**

`POST(/sign-up)
Registro para usuarios.`
<br>
`change body:`
<br>
`{
"username":"{your-username}",
"password":"{your-password}"
}`
_ _ _
`POST(/log-in)
Ingreso para usuarios.`
<br>
`change body:`
<br>
`{
"username":"{your-username}",
"password":"{your-password}"
}`
_ _ _
`POST(/log-out)
Desconexión para usuarios.`
_ _ _
`GET(/increments?valor1={valor}&valor2={valor})`
<br>
`- Calcula un incremento dado por un porcentual entre los anteriores dos valores. valor1 y valor2 obligatorios`
_ _ _
`GET(/request-audits?fechaHoraDesde={fechaDesde}&fechaHoraHasta={fechaHasta}&pagina={pagina})`
<br>
`- Obtiene el historial de http request exitosas del servidor.
fechaDesde y fechaHasta opcional.
pagina obligatorio.`
_ _ _
### External (puerto: 8081)

- **Requests**


`GET(/current-percentage)`
<br> 
`Obtiene un porcentaje actual usado en el calculo de /incrementos?valor1={valor}&valor2={valor}.`


_ _ _
_ _ _

## Instalación y Uso
Guía para **instalar**, **instanciar** y **utilizar** localmente los servidores.

1. **Tener docker instalado**. 
   1. En caso de no poseerlo, seguir la instalación desde el siguiente link [docker-install](https://docs.docker.com/engine/install/)
2. **Correr Docker**
   1. Desde la consola de comandos, ir hasta la carpeta donde se hizo la descarga del repositorio y moverse a `/tenpo-challenge`.
   2. Ejecutar el comando `docker compose up`.
3. **Probar desde Postman**
   1. En caso de no poseer postman debe descargarlo desde el siguiente link [postman-install](https://www.postman.com/downloads/).
   2. Desde la aplicación postman, ir a `import`, luego `file`, e importar la colección `tenpo-challenge.json` ubicada dentro del proyecto `/tenpo-challenge/docs/`.
   
Owner: **Luciano Reyes**